# About
You can find the source code from https://gobyexample.com/http-servers. This example is showing how to containerize it.

# To test is locally
You can just run the server by:
```bash
go run main.go
```
The server should be running, so that you can make a http request to test it.
```bash
curl localhost:8090/hello
```

# Build/Run with docker
```bash
docker build -t simple-go-http-server .
# docker build -t registry.gitlab.com/conan-chang/example-code/simple-go-http-server .
docker run --rm -p 8090:8090 simple-go-http-server
```