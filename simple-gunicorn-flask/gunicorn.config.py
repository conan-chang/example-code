"""Gunicorn configuration."""

bind = '0.0.0.0:8080'

workers = 1
threads = 2
worker_class = 'gthread'
# dont print access log
# accesslog = '-'
timeout = 180

