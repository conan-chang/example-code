# Build/Run
with nginx version:
```bash
docker build -t simple-gunicorn-flask .
docker run --rm -p 5080:5080 simple-gunicorn-flask
```

without nginx version:
```bash
docker build -t simple-gunicorn-flask-no-nginx -f Dockerfile.nonginx .
docker run --rm -p 8080:8080 simple-gunicorn-flask-no-nginx
```