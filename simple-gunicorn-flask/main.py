import time

from flask import Flask

app = Flask(__name__)


def hello():
    time.sleep(0.05)
    return 'hello'


app.add_url_rule('/', 'hello', hello)
app.add_url_rule('/hello', 'hello', hello)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
